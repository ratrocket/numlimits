# numlimits

> **(May 2021)** moved to [md0.org/numlimits](https://md0.org/numlimits). Use `import md0.org/numlimits`

Go package that defines constants for min/max values of different types.
Right now it's only ints...

Useful for having a starting value when iterating through a list looking
for the max (or min), eg:

```
max := numlimits.MinInt
for _, e := range l {
    if e > max {
        max = e
    }
}
```

The package could certainly be named better.
