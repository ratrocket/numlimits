// Package numlimits defines constants for min/max values of different
// types.  Right now it's only ints...
//
// Useful for having a starting value when iterating through a list
// looking for the max (or min), eg:
//
// max := numlimits.MinInt
// for _, e := range l {
//     if e > max {
//         max = e
//     }
// }
package numlimits

const (
	MaxInt = int(^uint(0) >> 1)
	MinInt = -MaxInt - 1
)
